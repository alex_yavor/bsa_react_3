import React from 'react';
import './App.css';
import Chat from './containers/chat';
import Spinner from './components/spinner';
import EditMessage from './containers/editMessage';
import { Switch, Route } from 'react-router-dom';
import UserPage from './containers/userPage';
import UserList from './containers/users';
import Login from './containers/login';
import ProtectedRoute from './components/protectedRoute';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true
    };
  }
  componentDidMount() {
    const loading = false;
    //for example
    setTimeout(() => this.setState({ loading }), 1000)

  }

  render() {
    const { loading } = this.state;
    if (loading) {
      return <Spinner />
    }
    return (
      <div>
        <Switch>
          <Route exact path='/login' component={Login} />
          <ProtectedRoute exact path='/' component={Chat} />
          <ProtectedRoute  path='/chat' component={Chat} />
          <ProtectedRoute exact path="/user" component={UserPage} />
          <ProtectedRoute exact path="/users" component={UserList} />
          <ProtectedRoute path="/user/:id" component={UserPage} />
          <ProtectedRoute path="/edit/:id" component={EditMessage} />
        </Switch>
      </div>
    )
  }
}


export default App;
