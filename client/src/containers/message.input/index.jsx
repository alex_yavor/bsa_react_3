import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux'
import './style.css';
import { sendMessage } from '../chat/actions';
import * as actions from './actions';
import { scrollToBottom } from '../message.list/actions';
class MessageInput extends React.Component {

    sendMessage = () => {
        const messageText = this.props.messageText.trim();
        this.props.dropMessageText();
        if (messageText === "") return;
        this.props.sendMessage(messageText);
        // setTimeout(this.props.scrollToBottom(), 100);
    }
    render() {
        const messageText = this.props.messageText;

        return (
            <div className='message-input'>

                <input
                    type="text"
                    placeholder='Message'
                    value={messageText}
                    onChange={this.props.handleChange} />
                <button
                    className='send-message-button'
                    onClick={() => this.sendMessage()}>
                    Send</button>
            </div>
        );
    }
}

MessageInput.propTypes = {
    sendMessage: PropTypes.func.isRequired,
    messageText: PropTypes.string,
    handleChange: PropTypes.func.isRequired,
    dropMessageText: PropTypes.func.isRequired
}

const mapStateToProps = (state) => {
    return {
        messageText: state.messageInput.messageText
    }
};

const mapDispatchToProps = {
    ...actions,
    sendMessage,
    scrollToBottom
};

export default connect(mapStateToProps, mapDispatchToProps)(MessageInput);