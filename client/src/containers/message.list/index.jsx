import React from 'react';
import Message from '../../components/message';
import MyMessage from '../../components/myMessage';
import DateLine from '../../components/dateLine';
import getFormattedTime from '../../helpers/timeFormat';
import PropTypes from 'prop-types';
import { connect } from 'react-redux'
import { likeMessage } from '../chat/actions';
import * as actions from './actions';

class MessageList extends React.Component {

    componentDidUpdate() {
        
    }

    renderMessages = () => {
        const { messages } = this.props;
        const listMessages = [];
        const messagesLength = messages.length;
        if (messagesLength)
            listMessages.push(<DateLine key={messages[0].created_at} time={new Date(messages[0].created_at)} ></DateLine>)
        messages.forEach((message, messageIndex) => {
            let messageDiv;
            if (message.isMyMessage) {
                messageDiv = <MyMessage
                    id={message.id}
                    key={message.id}
                    created_at={getFormattedTime(message.created_at)}
                    message={message.message}
                    likeCount={message.likeCount || 0}
                    isEditable={true}
                />
            }
            else {
                messageDiv = <Message
                    avatar={message.avatar}
                    created_at={getFormattedTime(message.created_at)}
                    message={message.message}
                    key={message.id}
                    user={message.user}
                    likeCount={message.likeCount || 0}
                    likeMessage={() => this.props.likeMessage(message.id)}
                />
            }
            listMessages.push(messageDiv);
            if (messageIndex !== 0 && messageIndex + 1 !== messages.length) {
                const startTime = new Date(message.created_at).getTime();
                const endTime = new Date(messages[messageIndex + 1].created_at).getTime();
                const compareTime = Math.round((new Date(endTime).getTime() - new Date(startTime).getTime()) / 1000 / 60 / 60 / 24)
                if (compareTime >= 1) {
                    listMessages.push(<DateLine key={endTime} time={new Date(endTime)} ></DateLine>)
                }
            }
        })
        return listMessages;
    }

    render() {

        return (
            <div
                className='message-list'
                ref={messageList => { this.props.initList(messageList); }}>
                {this.renderMessages()}
            </div>
        );
    }
}

MessageList.propTypes = {
    messages: PropTypes.array,
    likeMessage: PropTypes.func.isRequired
}

const mapStateToProps = (state) => {
    return {
        messages: state.chat.messages

    }
};

const mapDispatchToProps = {
    ...actions,
    likeMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(MessageList);