import { SCROLL_TO_BOTTOM, INIT_LIST } from "./actionTypes";


export const scrollToBottom = () => ({
    type: SCROLL_TO_BOTTOM,

});
export const initList = messageList => ({
    type: INIT_LIST,
    payload: {
        messageList
    }

});

