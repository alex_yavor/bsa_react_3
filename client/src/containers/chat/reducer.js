import { FETCH_MESSAGES_SUCCESS, LIKE_MESSAGE } from "./actionTypes";



const initialState = {
    lastId: 1,
    chatName: "My Chat"
}

export default function (state = initialState, action) {
    switch (action.type) {

        case FETCH_MESSAGES_SUCCESS: {

            const { messages } = action.payload;
            console.log('reducer', messages);
            return {
                ...state,
                messages
            };
        }

        case LIKE_MESSAGE: {
            const { id } = action.payload;
            let messages = state.messages.slice();
            let message = messages.filter((message) => message.id === id)[0];
            const isLiked = message.isLiked;
            if (!isLiked) {
                isNaN(message.likeCount) ? message.likeCount = 1 : message.likeCount++;
            }
            else { message.likeCount--; }
            message.isLiked = !isLiked;
            return {
                ...state,
                messages
            }
        }

        default:
            return state;
    }
}
