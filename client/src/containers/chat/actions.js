import { DELETE_MESSAGE, SEND_MESSAGE, EDIT_MESSAGE, LIKE_MESSAGE, FETCH_MESSAGES } from "./actionTypes";


// export const saveMessage = editedMessage => ({
//     type: SAVE_MESSAGE,
//     payload: {
//         editedMessage
//     }
// });

export const deleteMessage = id => ({
    type: DELETE_MESSAGE,
    payload: {
        id
    }
});

export const sendMessage = message => ({
    type: SEND_MESSAGE,
    payload: {
        message
    }
});

export const editMessage = (id, message) => ({
    type: EDIT_MESSAGE,
    payload: {
        id,
        message
    }
});

export const likeMessage = id => ({
    type: LIKE_MESSAGE,
    payload: {
        id
    }
});

export const fetchMessages = () => ({
    type: FETCH_MESSAGES
});

