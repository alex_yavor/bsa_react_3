import React from 'react';
import { connect } from 'react-redux'
import * as actions from './actions';
import Header from '../header/index';
import MessageList from '../message.list';
import MessageInput from '../message.input';
import getFromattedTime from '../../helpers/timeFormat';

import './style.css';

class Chat extends React.Component {

    componentDidMount() {
        this.props.fetchMessages();
     
    }

    render() {
        let { messages, chatName } = this.props;
        if (!messages) return null;
        let lastMessage = messages[messages.length - 1];
        let users = new Set(messages.map((message) => message.user));
        let userCount = users.size;
        return (
            <div className="chat" >
                <Header
                    chatName={chatName}
                    messageCount={messages.length}
                    lastMessageDate={getFromattedTime(lastMessage.created_at)}
                    userCount={userCount}
                />
                <MessageList messages={this.props.messages} />
                <MessageInput />
            </div >);
    }
}

const mapStateToProps = (state) => {
    return {
        ...state.chat,
    }
};

const mapDispatchToProps = {
    ...actions,
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
