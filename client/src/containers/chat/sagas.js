import axios from 'axios';
import api from '../../shared/config/api';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import { DELETE_MESSAGE, SEND_MESSAGE, EDIT_MESSAGE, LIKE_MESSAGE, FETCH_MESSAGES, FETCH_MESSAGES_SUCCESS } from "./actionTypes";
import { SCROLL_TO_BOTTOM } from '../message.list/actionTypes';

export function* fetchMessages() {
    try {
        const token = localStorage.getItem('jwt');
        const messages = yield call(axios.get, `${api.url}/message`, { headers: { "Authorization": `${token}` } });
        yield put({ type: FETCH_MESSAGES_SUCCESS, payload: { messages: messages.data } })
        yield put({ type: SCROLL_TO_BOTTOM })
    } catch (error) {
        console.log('fetchMessages error:', error.message)
    }
}

function* watchFetchMessages() {
    yield takeEvery(FETCH_MESSAGES, fetchMessages)
}

export function* sendMessage(action) {
    
    const message = action.payload.message;
    console.log(message)

    try {
        const token = localStorage.getItem('jwt');
        yield call(axios.post, `${api.url}/message`, { message }, { headers: { "Authorization": `${token}` } });
        yield put({ type: FETCH_MESSAGES });
    } catch (error) {
        console.log('sendMessage error:', error.message);
    }
}

function* watchSendMessage() {
    yield takeEvery(SEND_MESSAGE, sendMessage)
}

export function* editMessage(action) {

    const editedMessage = action.payload.message;
    const id = action.payload.id;
    console.log('edited', editedMessage);
    const token = localStorage.getItem('jwt');
    try {
        yield call(axios.put, `${api.url}/message/${id}`, editedMessage, { headers: { "Access-Control-Allow-Origin": "*", "Authorization": `${token}` } });
        yield put({ type: FETCH_MESSAGES });
    } catch (error) {
        console.log('editMessage error:', error.message);
    }
}

function* watchEditMessage() {
    yield takeEvery(EDIT_MESSAGE, editMessage)
}

export function* deleteMessage(action) {
    try {
        yield call(axios.delete, `${api.url}/message/${action.payload.id}`);
        yield put({ type: FETCH_MESSAGES })
    } catch (error) {
        console.log('deleteMessage Error:', error.message);
    }
}

function* watchDeleteMessage() {
    yield takeEvery(DELETE_MESSAGE, deleteMessage)
}

export default function* messagesSagas() {
    yield all([
        watchFetchMessages(),
        watchSendMessage(),
        watchEditMessage(),
        watchDeleteMessage()
    ])
};
