import { FETCH_MESSAGE_SUCCESS } from "./actionTypes";

const initialState = {

};

export default function (state = initialState, action) {
    switch (action.type) {

        case FETCH_MESSAGE_SUCCESS: {
            return {
                ...state,
                message: action.payload.message
            };
        }

        default:
            return state;
    }
}
