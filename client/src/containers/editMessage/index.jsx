import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux'
import * as actions from './actions';
import './style.css';
import { editMessage } from '../chat/actions';
class EditMessage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    componentDidMount(nextProps) {
        if (this.props.match.params.id) {
            this.props.fetchMessage(this.props.match.params.id)
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        console.log(nextProps)
        if (nextProps.message && (nextProps.message.id !== prevState.id) && nextProps.match.params.id) {
            return {
                ...nextProps.message
            };
        } else {
            return null;
        }
    }


    sendMessage = () => {
        console.log('send', this.state, this.state.id)
        const editedText = this.state.message.trim();
        if (editedText === "") {
            return;
        }
        this.setState({ message: '', id: -1 });
        this.props.editMessage(this.state.id, this.state);
        this.props.history.push('/chat');
    }

    onClose = () => {
        this.props.history.push('/chat');
        this.setState({ message: '', id: -1 });
    }

    handleChange = event => {
        const message = event.target.value;

        this.setState({ ...this.state, message });
    }

    render() {
        return (
            <div className="backdrop" >
                <div className="modal" >

                    <div className="footer">
                        <div>Edit message</div>
                        <input
                            type="text"
                            placeholder='Type something'
                            value={this.state.message}
                            onChange={this.handleChange} />
                        <button
                            onClick={this.onClose}>
                            Close
                        </button>
                        <button
                            onClick={this.sendMessage}>
                            Save
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}

EditMessage.propTypes = {
    sendMessage: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
    return {
        ...state.editMessage,
        message: state.editMessage.message
    }
};

const mapDispatchToProps = {
    ...actions,
    editMessage
};

export default connect(mapStateToProps, mapDispatchToProps)(EditMessage);