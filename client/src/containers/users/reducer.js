import { FETCH_USERS_SUCCESS, UNAUTHORIZED } from "./actionTypes";

export default function (state = [], action) {
    switch (action.type) {
        case FETCH_USERS_SUCCESS: {
            return [...action.payload.users];
        }
        case UNAUTHORIZED: {
            return {
                ...state,
                isUnauthorized: true
            };
        }


        default:
            return state;
    }
}
