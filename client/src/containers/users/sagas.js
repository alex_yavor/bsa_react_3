import axios from 'axios';
import api from '../../shared/config/api';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import { ADD_USER, UPDATE_USER, DELETE_USER, FETCH_USERS, FETCH_USERS_SUCCESS, UNAUTHORIZED } from "./actionTypes";
import React from 'react';
import { Route, Redirect } from 'react-router-dom';

export function* fetchUsers() {
	const token = localStorage.getItem('jwt');
	try {
		const users = yield call(axios.get, `${api.url}/user`, { headers: { "Authorization": `${token}` } });
		yield put({ type: FETCH_USERS_SUCCESS, payload: { users: users.data } })
	} catch (error) {
		yield put({ type:UNAUTHORIZED })
		console.log('fetchUsers error:', error.message)
	}
}

function* watchFetchUsers() {
	yield takeEvery(FETCH_USERS, fetchUsers)
}

export function* addUser(action) {
	const token = localStorage.getItem('jwt');
	const newUser = { ...action.payload.data, id: action.payload.id };
	console.log(newUser)
	try {
		yield call(axios.post, `${api.url}/user`, newUser, { headers: { "Authorization": `${token}` } });
		yield put({ type: FETCH_USERS });
	} catch (error) {
		console.log('createUser error:', error.message);
	}
}

function* watchAddUser() {
	yield takeEvery(ADD_USER, addUser)
}

export function* updateUser(action) {
	const token = localStorage.getItem('jwt');
	const id = action.payload.id;
	const updatedUser = { ...action.payload.data };
	console.log(updatedUser);
	try {
		yield call(axios.put, `${api.url}/user/${id}`, updatedUser, { headers: { "Authorization": `${token}` } });
		yield put({ type: FETCH_USERS });
	} catch (error) {
		console.log('updateUser error:', error.message);
	}
}

function* watchUpdateUser() {
	yield takeEvery(UPDATE_USER, updateUser)
}

export function* deleteUser(action) {
	const token = localStorage.getItem('jwt');
	try {
		yield call(axios.delete, `${api.url}/user/${action.payload.id}`, { headers: { "Authorization": `${token}` } });
		yield put({ type: FETCH_USERS })
	} catch (error) {
		console.log('deleteUser Error:', error.message);
	}
}

function* watchDeleteUser() {
	yield takeEvery(DELETE_USER, deleteUser)
}

export default function* usersSagas() {
	yield all([
		watchFetchUsers(),
		watchAddUser(),
		watchUpdateUser(),
		watchDeleteUser()
	])
};


