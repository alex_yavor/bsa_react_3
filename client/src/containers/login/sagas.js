import axios from 'axios';
import api from '../../shared/config/api';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import { LOGIN_SUCCESS, LOGIN } from "./actionTypes";

export function* login(action) {
    try {
        const { login, password } = action.payload;
        const response = yield call(axios.post, `${api.url}/login`, { login, password });
        yield put({ type: LOGIN_SUCCESS, payload: { token: response.data.token, isAdmin: response.data.isAdmin } })
    } catch (error) {
        console.log('login error:', error.message)
    }
}

function* watchLogin() {
    yield takeEvery(LOGIN, login)
}

export default function* loginSagas() {
    yield all([
        watchLogin()
    ])
};
