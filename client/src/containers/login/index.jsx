import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux'
// import './style.css'
import TextInput from '../../shared/inputs/text/TextInput';
import PasswordInput from '../../shared/inputs/password/PasswordInput';
import * as actions from './actions';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            password: "",
            login: ""
        }
    }
    onChangeData = (e, keyword) => {
        const value = e.target.value;
        this.setState(
            {
                ...this.state,
                [keyword]: value
            }
        );
    }
    componentDidUpdate(prevProps) {
        console.log('ad',this.props.isAdmin)
        if (prevProps.token !== this.props.token) {
            if (this.props.isAdmin)
                this.props.history.push('/users');
            else
                this.props.history.push('/chat');
        }
    }
    login = () => {
        const { login, password } = this.state;
        this.props.login(login, password);
    }

    render() {


        return (

            <div>
                <TextInput
                    key={1}
                    label="Login"
                    type="text"
                    text={this.state.login}
                    keyword="login"
                    onChange={this.onChangeData}
                />
                <PasswordInput
                    key={2}
                    label="Password"
                    type="password"
                    text={this.state.password}
                    keyword="password"
                    onChange={this.onChangeData}
                />
                <button onClick={this.login}>Log In</button>
            </div>
        )
    }
}

// Login.propTypes = {
//     messageCount: PropTypes.number,
//     lastMessageDate: PropTypes.string,
//     userCount: PropTypes.number,
//     chatName: PropTypes.string
// }
const mapStateToProps = (state) => {
    return {
        ...state.login
    }
};

const mapDispatchToProps = {
    ...actions,

};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
