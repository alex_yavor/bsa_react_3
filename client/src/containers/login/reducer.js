import { LOGIN_SUCCESS } from "./actionTypes";


export default function (state = [], action) {
    switch (action.type) {
        case LOGIN_SUCCESS: {
            const { token, isAdmin } = action.payload;
            localStorage.setItem('jwt', token);
            return {
                ...state,
                token, isAdmin

            };
        }

        default:
            return state;
    }
}
