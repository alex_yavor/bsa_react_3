import { LOGIN } from "./actionTypes";

export const login = (login, password) => ({
    type: LOGIN,
    payload: {
        login,
        password
    }
});