import { all } from 'redux-saga/effects';
import userPageSagas from '../containers/userPage/sagas';
import usersSagas from '../containers/users/sagas';
import messagesSagas from '../containers/chat/sagas';
import messageSagas from '../containers/editMessage/sagas';
import loginSagas from '../containers/login/sagas';
export default function* rootSaga() {
    yield all([
        userPageSagas(),
        usersSagas(),
        messagesSagas(),
        messageSagas(),
        loginSagas()
    ])
};