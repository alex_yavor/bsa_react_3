import React from 'react';
import PropTypes from 'prop-types';

import './style.css';

const Message = ({ id, avatar, message, created_at, user, likeMessage, likeCount }) => {
    return (
        <div
            className="message"
            key={id}>
            <img
                className="avatar"
                src={avatar}
                alt={user} />
            <div className="message-text">{message}</div>
            <div>{created_at}</div>
            <img
                onClick={likeMessage}
                className="icon hover"
                src='like.svg'
                alt="Like" />
            <span
                onClick={likeMessage}
                className="hover">
                {likeCount}
            </span>
        </div>
    )
}

Message.propTypes = {
    id: PropTypes.number,
    avatar: PropTypes.string,
    message: PropTypes.string,
    created_at: PropTypes.string,
    user: PropTypes.string,
    likeMessage: PropTypes.func.isRequired,
    likeCount: PropTypes.number
}


export default Message;