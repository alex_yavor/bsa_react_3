import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux'
import { deleteMessage, editMessage } from '../../containers/chat/actions';
import './style.css';
import { withRouter } from "react-router";
const MyMessage = ({ history, message, created_at, id, likeCount, isEditable, deleteMessage }) => {

    const edit = (id) => {
        history.push(`/edit/${id}`);
    }

    return (
        <div
            key={id}
            className="message right">
            <div className="message-text">{message}</div>
            <span>{created_at}</span>
            <div>
                <img
                    className="icon"
                    src='like.svg'
                    alt="Like" />
                <span>{likeCount}</span>
                <span
                    className="delete"
                    onClick={() => deleteMessage(id)}>
                    delete
                    </span>
                {isEditable ?
                    <span
                        className="edit"
                        onClick={() => edit(id)}>
                        edit
                        </span> : null}
            </div>
        </div>
    )

}

MyMessage.propTypes = {
    message: PropTypes.string,
    created_at: PropTypes.string,
    id: PropTypes.string,
    deleteMessage: PropTypes.func.isRequired,
    editMessage: PropTypes.func.isRequired,
    likeCount: PropTypes.number,
    isEditable: PropTypes.bool
}
const mapStateToProps = (state) => {
    return {}
};

const mapDispatchToProps = {
    deleteMessage,
    editMessage,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MyMessage));
