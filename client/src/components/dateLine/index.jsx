import React from 'react';
import PropTypes from 'prop-types';

import './style.css';

const DateLine = ({ time }) => {
    const month = time.toLocaleDateString('en-US', { month: 'long' });
    const day = time.getDate();
    const date = `${month} ${day}`
    return (
        <div className="title title-center">{date}</div>
    );

}

DateLine.propTypes = {
    time: PropTypes.object
}

export default DateLine;