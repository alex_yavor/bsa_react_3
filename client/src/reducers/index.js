import { combineReducers } from "redux";
import chat from "../containers/chat/reducer";
import editMessage from "../containers/editMessage/reducer";
import messageInput from "../containers/message.input/reducer";
import messageList from "../containers/message.list/reducer";
import users from "../containers/users/reducer";
import userPage from "../containers/userPage/reducer";
import login from "../containers/login/reducer";
const rootReducer = combineReducers({
    chat,
    editMessage,
    messageInput,
    messageList,
    users,
    userPage,
    login
})

export default rootReducer;
