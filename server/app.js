const express = require('express');
const bodyPaser = require('body-parser');
const logger = require('morgan');
const cors = require('cors');
const app = express();
const passport = require('passport');
const users = require('./data/userlist');
const jwt = require('jsonwebtoken');

app.use(logger('dev'));

app.use(cors());
app.use(bodyPaser.urlencoded({ extended: true }))
app.use(bodyPaser.json())


app.post('/api/login', function (req, res) {
  const userFromReq = req.body;
  console.log(userFromReq);
  let isAdmin = false
  const userInDB = users.find(user => user.name === userFromReq.login);
  if (userInDB && userInDB.password === userFromReq.password) {
    const token = jwt.sign(userFromReq, 'someSecret', { expiresIn: '24h' });
    if (userFromReq.password == 'admin' && userFromReq.login == 'admin')
     { isAdmin = true;console.log('isadmin')}
    res.status(200).json({ auth: true, token, isAdmin });
  } else {
    res.status(401).json({ auth: false });
  }
});

const routes = require('./routes');
routes(app);

require('./middlewares/passport.config');






const server = require('http').Server(app);


app.use(passport.initialize());

server.listen(3001);
app.post('api/auth/login', function (req, res, next) {
  const userFromReq = req.body;
  const userInDB = users.find(user => user.login === userFromReq.login);
  if (userInDB && userInDB.password === userFromReq.password) {
    const token = jwt.sign(userFromReq, 'someSecret', { expiresIn: '24h' });
    res.status(200).json({ auth: true, token });
  } else {
    res.status(401).json({ auth: false });
  }
});


//
// var cookieParser = require('cookie-parser');
// var indexRouter = require('./routes/index');
// var usersRouter = require('./routes/users');
// app.use(cookieParser());

//
// module.exports = app;
module.exports = server;
