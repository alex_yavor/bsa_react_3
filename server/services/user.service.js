const userRepository = require("../repositories/user.repository");

const getUsers = async () => {
  try {
    const users = await userRepository.getAllUsers();
    if (users) {
      return users;
    }
    else {
      return null;
    }
  }
  catch (error) {
    console.log(error.message);
  }
}

const getUser = async (userId) => {
  try {
    const user = await userRepository.getUserInfo(userId);
    if (user) {
      console.log('getUser service', user)
      return user;
    }
    else {
      return null;
    }
  }
  catch (error) {
    console.log(error.message);
  }
}

const createUser = async (user) => {
  try {

    const response = await userRepository.createUser(user);
    console.log('resp',response);
    if (response) {

      return response;
    }
    else {
      return "Can not create user";
    }
  }
  catch (error) {
    console.log(error.message);
  }
}

const updateUser = async (user,id) => {

  try {
    const response = await userRepository.updateUser(user,id);

    return response;
  }
  catch (error) {
    console.log(error.message);

    return "Can not update user";
  }
}

const deleteUser = async (userId) => {
  try {
    let response = await userRepository.deleteUser(userId);

    return response;
  }
  catch (error) {
    console.log(error.message);

    return "Can not delete user";
  }
}


module.exports = {
  getUsers,
  getUser,
  createUser,
  updateUser,
  deleteUser
};
