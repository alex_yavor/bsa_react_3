const messageRepository = require("../repositories/message.repository");
const jwt = require('jsonwebtoken');
const getMessages = async (auth) => {
  const user = jwt.decode(auth).login;
  try {
    let messages = await messageRepository.getMessages();
    messages = messages.map(message => {
      if (message.user == user) {
        message.isMyMessage = true;
      }
      return message;
    })
    if (messages) {
      return messages;
    }
    else {
      return null;
    }
  }
  catch (error) {
    console.log(error.message);
  }
}

const getMessage = async (messageId) => {
  try {
    const message = await messageRepository.getMessage(messageId);
    if (message) {
      return message;
    }
    else {
      return null;
    }
  }
  catch (error) {
    console.log(error.message);
  }
}

const createMessage = async (req) => {
  const name = jwt.decode(req.headers['authorization']).login;
  const message = req.body.message;
  console.log('create service', message)
  try {


    const response = await messageRepository.createMessage(name, message);
    if (response) {

      return response;
    }
    else {
      return "Can not create user";
    }
  }
  catch (error) {
    console.log(error.message);
  }
}

const updateMessage = async (message, id) => {
  console.log('updateMessage service', message)
  try {
    
    const response = await messageRepository.updateMessage(message, id);

    return response;
  }
  catch (error) {
    console.log(error.message);

    return "Can not update user";
  }
}

const deleteMessage = async (messageId) => {
  try {
    let response = await messageRepository.deletemessage(messageId);

    return response;
  }
  catch (error) {
    console.log(error.message);

    return "Can not delete message";
  }
}

//   const InvalidUserParams = ({ health, attack, defense }) => {
//     console.log('fff',health,defense)
//     if (isNaN(health) || health < 1) {
//       return "Invalid health";
//     }
//     if (isNaN(attack) || attack < 1) {
//       return "Invalid attack";
//     }
//     if (isNaN(defense) || defense < 0) {
//       return "Invalid defense";
//     }
//     return false;
//   }

module.exports = {
  getMessages,
  getMessage,
  createMessage,
  updateMessage,
  deleteMessage
};
