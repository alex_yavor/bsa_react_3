# Street fighter game

## Credits 

Thanks [Binary Studio Academy](https://github.com/binary-studio-academy/stage-2-express-yourself-with-nodejs) for initial commit

##Homepage

[Heroku server](https://street-fighter-bsa.herokuapp.com/)

## Instalation

`npm install`

`npm run start`

open http://localhost:3000/

## License
[MIT](https://choosealicense.com/licenses/mit/)
