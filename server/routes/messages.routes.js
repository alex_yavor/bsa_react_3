const express = require('express');
const router = express.Router();

const messageService = require("../services/message.service");

router.get('/', async (req, res, next) => messageService.getMessages(req.headers['authorization'])
  .then(response => res.send(response)).catch(next));

router.get('/:id', (req, res, next) => messageService.getMessage(req.params.id)
  .then(response => res.send(response)).catch(next));

router.post('/', async (req, res, next) => messageService.createMessage(req)
  .then(response => res.send(response)).catch(next));

router.put('/:id', (req, res, next) => messageService.updateMessage(req.body, req.params.id)
  .then(response => res.send(response)).catch(next));

router.delete('/:id', async (req, res, next) => messageService.deleteMessage(req.params.id)
  .then(response => res.send(response)).catch(next));

module.exports = router;
