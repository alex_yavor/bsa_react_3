const messagesRouter = require('./messages.routes');
const usersRouter = require('./users.routes');

const routes = (app) => {
  app.use('/api/message', messagesRouter);
  app.use('/api/user', usersRouter);
};

module.exports = routes;
