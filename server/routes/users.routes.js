const express = require('express');
const router = express.Router();
const isAdmin = require('../middlewares/auth.middleware').isAdmin;
const {
  getUsers,
  getUser,
  createUser,
  updateUser,
  deleteUser } = require("../services/user.service");

router.get('/',isAdmin, async (req, res, next) => {
  const users = await getUsers();
  if (users) {
    res.send(users);
  } else {
    res.status(400).send(`Can not find users`);
  }
});

router.get('/:id', isAdmin, async (req, res, next) => {
  const userId = req.params.id;
  const user = await getUser(userId);
  if (user) {
    res.send(user);
  } else {
    res.status(400).send(`Can not find user with id ${userId}`);
  }

});

router.post('/',isAdmin, async (req, res, next) => {
  const user = req.body;
  console.log('post', req.body)
  const answer = await createUser(user)
  res.send(answer);
});

router.put('/:id',isAdmin, async (req, res, next) => {
  const user = req.body;
  console.log("put", req.body)
  const userId = req.params.id;
  const response = await updateUser(user, req.params.id);
  res.send({ response });

});

router.delete('/:id',isAdmin, async (req, res, next) => {
  const userId = req.params.id;
  const response = await deleteUser(userId);
  res.send(response);
});



module.exports = router;
