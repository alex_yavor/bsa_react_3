const jwt = require('jsonwebtoken');
const isAdmin = (req, res, next) => {
  if (
    req &&
    req.headers &&
    req.headers['authorization'] &&
    jwt.decode(req.headers['authorization']).login === 'admin' &&
    jwt.decode(req.headers['authorization']).password === 'admin'
  ) {
    next();
  } else {
    res.status(401).send()
  }
};

module.exports = {
  isAdmin
}