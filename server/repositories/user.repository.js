const fs = require('fs');

const getAllUsers = async () => {
  let jsonUsers = null;
  try {
    const data = await fs.promises.readFile('./data/userlist.json')
    jsonUsers = JSON.parse(data.toString())
    return jsonUsers;
  }
  catch (error) {
    console.log(error.message);
    return null;
  }
}

const getUserInfo = async (userId) => {
  let userInfo = null;

  try {
    const jsonUsers = await getAllUsers();

    jsonUsers.forEach((user) => {
      if (user && user.id == userId) {
        userInfo = user;
      }
    })
    return userInfo;
  }
  catch (error) {
    console.log(error.message);
    return null;
  }
}

const createUser = async (user) => {
  let response = undefined;
  console.log('create', user);
  try {
    let jsonUsers = await getAllUsers();
    console.log('create', user);
    jsonUsers.push(user);
    const users = JSON.stringify(jsonUsers);
    await fs.promises.writeFile('./data/userlist.json', users);
    response = "User created";


  }
  catch (error) {
    response = "Cannot create user";
  }
  finally {
    return response;
  }
}

const updateUser = async (updatedUser, id) => {
  let response = undefined;
  let isFound = false;
  try {
    const jsonUsers = await getAllUsers();
    jsonUsers.forEach((user, index) => {
      if (user && user.id == id) {
        isFound = true;
        jsonUsers[index] = updatedUser;
      }
    })
    if (isFound) {
      const users = JSON.stringify(jsonUsers);
      await fs.promises.writeFile('./data/userlist.json', users);
      response = "User updated";
    }
    else {
      response = "User does not exist";
    }
  }
  catch (error) {
    console.log(error.message);
    response = "Can not update user";
  }
  finally {
    return response;
  }
}

const deleteUser = async (userId) => {
  let response = undefined;
  let isFound = false;
  try {
    const jsonUsers = await getAllUsers();
    const newJsonUsers = jsonUsers.filter((userInfo, index) => {
      if (userInfo && userInfo.id == userId) {
        isFound = true;
        return false;
      }
      else {
        return true;
      }
    })
    if (isFound) {
      const users = JSON.stringify(newJsonUsers);
      await fs.promises.writeFile('./data/userlist.json', users);
      response = "User deleted";
    }
    else {
      response = `User with id ${userId} not found`;
    }

  }
  catch (error) {
    console.log(error.message);
    response = "Can not delete user";
  }
  finally {
    return response;
  }
}

module.exports = {
  getAllUsers,
  getUserInfo,
  createUser,
  updateUser,
  deleteUser

};