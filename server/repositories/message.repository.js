const fs = require('fs');
const userRepository = require('./user.repository');
const getMessages = async () => {
    let jsonMessages = null;
    try {
        const data = await fs.promises.readFile('./data/messages.json')
        jsonMessages = JSON.parse(data.toString())
        return jsonMessages;
    }
    catch (error) {
        console.log(error.message);
        return null;
    }
}

const getMessage = async (messageId) => {
    let returningMessage = null;

    try {
        const jsonMessages = await getMessages();

        jsonMessages.forEach((message) => {
            if (message && message.id == messageId) {
                returningMessage = message;
            }
        })
        console.log('getmessage repo',returningMessage);
        return returningMessage;
    }
    catch (error) {
        console.log(error.message);
        return null;
    }
}

const createMessage = async (name, message) => {
    console.log('createMessage mesasge', message)
    try {
        let jsonMessages = await getMessages();
        let user = (await userRepository.getAllUsers()).find(user => user.name == name);
        console.log('createMessage user', user)
        const id = jsonMessages[jsonMessages.length - 1].id + 1;
        jsonMessages.push({
            message,
            id,
            user: user.name,
            marked_read: false,
            avatar: user.avatar,
            created_at: new Date()
        });
        const messages = JSON.stringify(jsonMessages);
        await fs.promises.writeFile('./data/messages.json', messages);
        return true;
    }
    catch (error) {
        console.log(error.message);
        return false;
    }

}

const updateMessage = async (updatedMessage, id) => {
    let response = true;
    try {
        let jsonMessages = await getMessages();
        jsonMessages.forEach((message, index) => {
            if (message.id == id) {

                jsonMessages[index] = updatedMessage;
            }
        })
        const messages = JSON.stringify(jsonMessages);
        await fs.promises.writeFile('./data/messages.json', messages);
        response = true;
    }
    catch (error) {
        console.log(error.message);
        response = false;
    }
    finally {
        return response;
    }
}

const deletemessage = async (messageId) => {
    let response = undefined;
    try {
        let jsonMessages = await getMessages();
        const updatedMessages = jsonMessages.filter((message, index) => {
            if (message && message.id == messageId) {
                return false;
            }
            else {
                return true;
            }
        })
        const messages = JSON.stringify(updatedMessages);
        await fs.promises.writeFile('./data/messages.json', messages);

        response = true;
    }
    catch (error) {
        console.log(error.message);
        response = false
    }
    finally {
        return response;
    }
}


module.exports = {
    getMessages,
    getMessage,
    updateMessage,
    deletemessage,
    createMessage
}